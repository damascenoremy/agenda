﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Agenda_app.Migrations
{
    /// <inheritdoc />
    public partial class agendaUpdate : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Agenda",
                type: "longtext",
                nullable: false);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Name",
                table: "Agenda");
        }
    }
}
