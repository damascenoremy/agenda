﻿using Agenda_app.Entities.ContactDetails;

namespace Agenda_app.Entities
{
    public class Contact
    {
        public int Id { get; set; }
        public string Name { get; set; } 
        public Address Address { get; set; }
        public Email Email { get; set; }
        public Phone Phone { get; set; }
        public Website Website { get; set; }
    }
}