﻿using Agenda_app.Entities;
using Agenda_app.Services;
using Microsoft.AspNetCore.Mvc;

namespace Agenda_app.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ContactController : ControllerBase
    {
        public readonly ContactService contactService;

        public ContactController(ContactService contactService)
        {
            this.contactService = contactService;
        }

        [HttpGet("{agendaId}")]
        public async Task<IActionResult> GetContactsFromAgenda(int agendaId)
        {
            try
            {
                return Ok(await contactService.GetContactsFromAgenda(agendaId));
            }
            catch (Exception exception)
            {
                return StatusCode(500, exception.Message);
            }
        }

        [HttpGet("{contactId}")]
        public async Task<IActionResult> GetContact(int contactId)
        {
            try
            {
                return Ok(await contactService.GetContact(contactId));
            }
            catch (Exception exception)
            {
                return StatusCode(500, exception.Message);
            }
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteContact(int contactId)
        {
            try
            {
                await contactService.DeleteContact(contactId);
                return Ok(StatusCodes.Status200OK);
            }
            catch (Exception exception)
            {
                return StatusCode(500, exception.Message);
            }
        }

        [HttpPut]
        public async Task<IActionResult> UpdateContact([FromBody]Contact contact)
        {
            try
            {
                await contactService.UpdateContact(contact);
                return CreatedAtAction(nameof(GetContact), new {id = contact.Id}, contact);
            }
            catch (Exception exception)
            {
                return StatusCode(500, exception.Message);
            }
        }
    }
}
