﻿using Agenda_app.Entities;
using Microsoft.EntityFrameworkCore;

namespace Agenda_app
{
    public class AgendaDbContext : DbContext
    {
        public DbSet<Agenda> Agenda { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Users> Users { get; set; }
        
        public AgendaDbContext(DbContextOptions<AgendaDbContext> options): base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new AgendaConfiguration());
            modelBuilder.ApplyConfiguration(new ContactConfiguration());
            modelBuilder.ApplyConfiguration(new UsersConfiguration());

            base.OnModelCreating(modelBuilder);
        }
    }
}
