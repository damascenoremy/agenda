﻿using System.Text.RegularExpressions;

namespace Agenda_app.Entities.ContactDetails
{
    public class Email : IContactController
    {
        public string Value { get; set; }
        public bool Validate()
        {
            Regex regex = new Regex(@"(\w |\.)+.@.*[a - z].\.[a-z]{ 2,}");
            
            if (regex.IsMatch(Value)) return true;
            else return false;
        }
    }
}