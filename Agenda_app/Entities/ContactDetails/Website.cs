﻿using System.Text.RegularExpressions;

namespace Agenda_app.Entities.ContactDetails
{
    public class Website:IContactController 
    {
        public string Value { get; set; }
        public bool Validate()
        {
            Regex regex = new Regex(@"(http://|https://)\w*\.\w{2,}");

            if (regex.IsMatch(Value)) return true;
            else return false;
        }
    }
}
