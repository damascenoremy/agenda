# Agenda

L'application Agenda a été réalisée avec .NET Core et Entity Framework Core
La base de donnée, générée automatiquement avec Entity Framework est composée de 4 tables :
\- Users
\- Contact
\- Agenda
\- __EFMigrationsHistory

![](/images/database.png)

La table __EFMigrationsHistory est une table générée par Entity Framework Core qui garde un historique des migrations.

Pour lancer le projet avec Visual Studio
Ouvrir la solution avec le fichier .sln
Lancer le projet depuis visual studio, un navigateur s'ouvrira avec Swagger UI.

Pour tester la création d'un item dans la base de données, enlever les lignes 'id' au niveau du JSON et cliquer sur execute.
Si un code 201 est retourné, la création s'est bien passée

Pour tester l'update d'un item, copier le JSON d'un item qui a été get, et le coller à l'endroit voulu en faisant les modifications désirées.

Pour delete ou get un item précis, préciser l'id de celui-ci.

![](/images/diagramme_de_classe.png)