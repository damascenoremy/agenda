﻿using System.Text.RegularExpressions;

namespace Agenda_app.Entities.ContactDetails
{
    public class Address : IContactController
    {
        public string Value { get; set; }
        public bool Validate()
        {
            // street number, street name, zip, city
            // example : 25 rue de la paix 35000 Rennes
            Regex regex = new Regex(@"(\w+\s*\w*)*\s\d{5}\s\w+");

            if (regex.IsMatch(Value)) return true;
            else return false;
        }
    }
}