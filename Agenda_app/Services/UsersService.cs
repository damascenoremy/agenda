﻿using Agenda_app.Entities;
using Microsoft.EntityFrameworkCore;

namespace Agenda_app.Services
{
    public class UsersService
    {
        public readonly AgendaDbContext _dbContext;

        public UsersService(AgendaDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task createUser(Users user)
        {
            await _dbContext.Users.AddAsync(user);
            await _dbContext.SaveChangesAsync();
        }
        public async Task<Users> getUser(int id)
        {
            Users user = await _dbContext.Users.FirstOrDefaultAsync(u => u.Id == id);

            return user;
        }
        public async Task<List<Users>> getUsers()
        {
            List<Users> users = _dbContext.Users.Select(u => u).ToList();

            return users;
        }
        public async Task updateUser(Users user)
        {
            Users userToUpdate = await _dbContext.Users.FirstOrDefaultAsync(u => u.Id == user.Id);

            userToUpdate.Login = user.Login;
            userToUpdate.Agendas = user.Agendas;
            userToUpdate.Password = user.Password;

            _dbContext.Users.Update(userToUpdate);
            await _dbContext.SaveChangesAsync();
        }
        public async Task deleteUser(int id)
        {
            Users userToDelete= await _dbContext.Users.FirstOrDefaultAsync(u => u.Id == id);

            _dbContext.Users.Remove(userToDelete);
            await _dbContext.SaveChangesAsync();
        }
    }
}
