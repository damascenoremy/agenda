﻿using Agenda_app.Entities;
using Microsoft.EntityFrameworkCore;

namespace Agenda_app.Services
{
    public class ContactService
    {
        public readonly AgendaDbContext _dbContext;

        public ContactService (AgendaDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task CreateContact(Contact contact)
        {
            await _dbContext.Contacts.AddAsync(contact);
            await _dbContext.SaveChangesAsync();
        }
        public async Task<Contact> GetContact(int contactId)
        {
            Contact contact = _dbContext.Contacts.FirstOrDefault(c => c.Id == contactId);

            return contact;
        }
        public async Task<List<Contact>> GetContactsFromAgenda(int agendaId)
        {
            List<Contact> contacts = _dbContext.Agenda.FirstOrDefault(a => a.Id == agendaId).Contacts.ToList();

            return contacts;
        }
        public async Task UpdateContact(Contact contact)
        {
            Contact contactToUpdate = await _dbContext.Contacts.FirstOrDefaultAsync(c => c.Id == contact.Id);

            contactToUpdate.Name = contact.Name;
            contactToUpdate.Address = contact.Address;
            contactToUpdate.Phone = contact.Phone;
            contactToUpdate.Email = contact.Email;

            _dbContext.Contacts.Update(contactToUpdate);
            await _dbContext.SaveChangesAsync();
        }
        public async Task DeleteContact(int id)
        {
            Contact contactToDelete = await _dbContext.Contacts.FirstOrDefaultAsync(c => c.Id == id);

            _dbContext.Contacts.Remove(contactToDelete);
            await _dbContext.SaveChangesAsync();
        }
    }
}
