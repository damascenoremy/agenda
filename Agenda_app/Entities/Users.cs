﻿namespace Agenda_app.Entities
{
    public class Users
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string JWT { get; set; }

        public IEnumerable<Agenda> Agendas { get; set; }
    }
}
