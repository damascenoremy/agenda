﻿using System.Text.RegularExpressions;

namespace Agenda_app.Entities.ContactDetails
{
    public class Phone : IContactController
    {
        public string Value { get; set; }
        public bool Validate()
        {
            Regex regex = new Regex(@"(\d{10})");

            if (Value.Contains(" ")) Value = Value.Substring(0, Value.IndexOf(" "));

            if (regex.IsMatch(Value)) return true;
            else return false;
        }
    }
}
