﻿using Agenda_app.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Agenda_app
{
    internal class UsersConfiguration : IEntityTypeConfiguration<Users>
    {
        public void Configure(EntityTypeBuilder<Users> builder)
        {
            builder.HasKey(u => u.Id);
            builder.Property(u => u.Id).ValueGeneratedOnAdd();
            builder.HasMany(u => u.Agendas).WithOne();
        }
    }

    internal class ContactConfiguration : IEntityTypeConfiguration<Contact>
    {
        public void Configure(EntityTypeBuilder<Contact> builder)
        {
            builder.HasKey(c => c.Id);
            builder.Property(u => u.Id).ValueGeneratedOnAdd();
            builder.OwnsOne(c => c.Address);
            builder.OwnsOne(c => c.Website);
            builder.OwnsOne(c => c.Phone);
            builder.OwnsOne(c => c.Email);
        }
    }

    internal class AgendaConfiguration : IEntityTypeConfiguration<Agenda>
    {
        public void Configure(EntityTypeBuilder<Agenda> builder)
        {
            builder.HasKey(a => a.Id);
            builder.Property(a => a.Id).ValueGeneratedOnAdd();
            builder.HasMany(a => a.Contacts).WithOne();
        }
    }
}
