﻿using Agenda_app.Services;
using Microsoft.AspNetCore.Mvc;

namespace Agenda_app.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AgendaController : ControllerBase
    {
        public readonly AgendaService agendaService;

        public AgendaController(AgendaService agendaService)
        {
            this.agendaService = agendaService;
        }
    }
}
