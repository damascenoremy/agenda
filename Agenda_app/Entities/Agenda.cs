﻿namespace Agenda_app.Entities
{
    public class Agenda
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<Contact> Contacts { get; set; }
    }
}